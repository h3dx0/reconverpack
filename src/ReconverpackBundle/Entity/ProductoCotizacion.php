<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * ProductoCotizacion
 *
 * @ORM\Table(name="producto_cotizacion")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\ProductoCotizacionRepository")
 */
class ProductoCotizacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="text", nullable=true)
     */
    private $observacion;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="cotizaciones")
     */
    private $producto;



    /**
     * ProductoCotizacion constructor.
     */
    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return ProductoCotizacion
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set medida
     *
     * @param float $medida
     *
     * @return ProductoCotizacion
     */
    public function setMedida($medida)
    {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get medida
     *
     * @return float
     */
    public function getMedida()
    {
        return $this->medida;
    }

    /**
     * Set capas
     *
     * @param integer $capas
     *
     * @return ProductoCotizacion
     */
    public function setCapas($capas)
    {
        $this->capas = $capas;

        return $this;
    }

    /**
     * Get capas
     *
     * @return integer
     */
    public function getCapas()
    {
        return $this->capas;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return ProductoCotizacion
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return ProductoCotizacion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set producto
     *
     * @param \ReconverpackBundle\Entity\Producto $producto
     *
     * @return ProductoCotizacion
     */
    public function setProducto(\ReconverpackBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \ReconverpackBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }

}
