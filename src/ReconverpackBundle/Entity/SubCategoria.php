<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubCategoria
 *
 * @ORM\Table(name="sub_categoria")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\SubCategoriaRepository")
 */
class SubCategoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
    * @var string
    *
    * @ORM\Column(name="descripcion", type="string", length=255)
    */
    private $descripcion;

    /**
    * @ORM\ManyToOne(targetEntity="Categoria")
    */
    private $categoria;

        public function __toString(){
          return "Categ:".$this->categoria."-SubCateg:".$this->nombre;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return SubCategoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return SubCategoria
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set categoria
     *
     * @param \ReconverpackBundle\Entity\Categoria $categoria
     * @return SubCategoria
     */
    public function setCategoria(\ReconverpackBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \ReconverpackBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }
}
