<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\ProductoPedido;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Productopedido controller.
 *
 */
class ProductoPedidoController extends Controller
{
    /**
     * Lists all productoPedido entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $productoPedidos = $em->getRepository('ReconverpackBundle:ProductoPedido')->findAll();

        return $this->render('productopedido/index.html.twig', array(
            'productoPedidos' => $productoPedidos,
        ));
    }

    /**
     * Creates a new productoPedido entity.
     *
     */
    public function newAction(Request $request)
    {
        $productoPedido = new Productopedido();
        $form = $this->createForm('ReconverpackBundle\Form\ProductoPedidoType', $productoPedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($productoPedido);
            $em->flush();

            return $this->redirectToRoute('productopedido_show', array('id' => $productoPedido->getId()));
        }

        return $this->render('productopedido/new.html.twig', array(
            'productoPedido' => $productoPedido,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a productoPedido entity.
     *
     */
    public function showAction(ProductoPedido $productoPedido)
    {
        $deleteForm = $this->createDeleteForm($productoPedido);

        return $this->render('productopedido/show.html.twig', array(
            'productoPedido' => $productoPedido,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing productoPedido entity.
     *
     */
    public function editAction(Request $request, ProductoPedido $productoPedido)
    {
        $deleteForm = $this->createDeleteForm($productoPedido);
        $editForm = $this->createForm('ReconverpackBundle\Form\ProductoPedidoType', $productoPedido);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('productopedido_edit', array('id' => $productoPedido->getId()));
        }

        return $this->render('productopedido/edit.html.twig', array(
            'productoPedido' => $productoPedido,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a productoPedido entity.
     *
     */
    public function deleteAction(Request $request, ProductoPedido $productoPedido)
    {
        $form = $this->createDeleteForm($productoPedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($productoPedido);
            $em->flush();
        }

        return $this->redirectToRoute('productopedido_index');
    }

    /**
     * Creates a form to delete a productoPedido entity.
     *
     * @param ProductoPedido $productoPedido The productoPedido entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProductoPedido $productoPedido)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('productopedido_delete', array('id' => $productoPedido->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
