<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PrecioProdCliente
 *
 * @ORM\Table(name="precio_prod_cliente")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\PrecioProdClienteRepository")
 */
class PrecioProdCliente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float")
     */
    private $precio;

    /**
     * @ORM\ManyToOne(targetEntity="Producto")
     */
    private $producto;

    public function __toString(){
      return $this->producto."-".$this->cliente."- $".$this->precio;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return PrecioProdCliente
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set producto
     *
     * @param \ReconverpackBundle\Entity\Producto $producto
     * @return PrecioProdCliente
     */
    public function setProducto(\ReconverpackBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \ReconverpackBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set cliente
     *
     * @param \ReconverpackBundle\Entity\Cliente $cliente
     * @return PrecioProdCliente
     */
    public function setCliente(\ReconverpackBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \ReconverpackBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}
