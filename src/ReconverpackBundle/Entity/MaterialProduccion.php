<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaterialProduccion
 *
 * @ORM\Table(name="material_produccion")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\MaterialProduccionRepository")
 */
class MaterialProduccion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;
    /**
     * @ORM\ManyToOne(targetEntity="Material")
     *@ORM\JoinColumn(name="material_id", referencedColumnName="id")
     */
    private $material;
    /**
     * @ORM\ManyToOne(targetEntity="OrdenProduccion", inversedBy="materiales")
     *@ORM\JoinColumn(name="ordenproduccion_id", referencedColumnName="id")
     */
    private $ordenProduccion;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return MaterialProduccion
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set material
     *
     * @param \ReconverpackBundle\Entity\Material $material
     * @return MaterialProduccion
     */
    public function setMaterial(\ReconverpackBundle\Entity\Material $material = null)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return \ReconverpackBundle\Entity\Material
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set ordenProduccion
     *
     * @param \ReconverpackBundle\Entity\OrdenProduccion $ordenProduccion
     * @return MaterialProduccion
     */
    public function setOrdenProduccion(\ReconverpackBundle\Entity\OrdenProduccion $ordenProduccion = null)
    {
        $this->ordenProduccion = $ordenProduccion;

        return $this;
    }

    /**
     * Get ordenProduccion
     *
     * @return \ReconverpackBundle\Entity\OrdenProduccion
     */
    public function getOrdenProduccion()
    {
        return $this->ordenProduccion;
    }
}
