<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\Pedido;
use ReconverpackBundle\Entity\ProductoPedido;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Pedido controller.
 *
 */
class PedidoController extends Controller {

    /**
     * Lists all pedido entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $pedidos = $em->getRepository('ReconverpackBundle:Pedido')->findBy(array(), array('fechaRegistro' => 'DESC'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $pedidos, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 15/* limit per page */
        );
        return $this->render('pedido/index.html.twig', array(
                    'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new pedido entity.
     *
     */
    public function newAction(Request $request) {
        $pedido = new Pedido();
        $form = $this->createForm('ReconverpackBundle\Form\PedidoType', $pedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pedido->setEstado('recibido');
            $em = $this->getDoctrine()->getManager();
            $em->persist($pedido);
            $em->flush();

            return $this->redirectToRoute('pedido_index');
        }

        return $this->render('pedido/new.html.twig', array(
                    'pedido' => $pedido,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pedido entity.
     *
     */
    public function showAction(Pedido $pedido) {
        $deleteForm = $this->createDeleteForm($pedido);

        return $this->render('pedido/show.html.twig', array(
                    'pedido' => $pedido,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pedido entity.
     *
     */
    public function editAction(Request $request, Pedido $pedido) {
        $deleteForm = $this->createDeleteForm($pedido);
        $editForm = $this->createForm('ReconverpackBundle\Form\PedidoEditarType', $pedido);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($pedido->getEstado() == 'cancelado') {
                $ordenes = $em->getRepository('ReconverpackBundle:OrdenProduccion')->findAll(array('pedido' => $pedido));
                foreach ($ordenes as $orden) {
                    $orden->setEstado('cancelado');
                }
            }
            if ($pedido->getEstado() == 'terminado') {
                $pedido->setFechaTerminadoProduccion(new \DateTime("now"));
            }
            if ($pedido->getEstado() == 'enviado') {
                $pedido->setFechaEnvio(new \DateTime("now"));
            }
            $em->flush();

            return $this->redirectToRoute('pedido_index');
        }

        return $this->render('pedido/edit.html.twig', array(
                    'pedido' => $pedido,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    public function editProdAction(Request $request, $id, $id_producto) {

        $em = $this->getDoctrine()->getManager();

        $pedido = $em->getRepository('ReconverpackBundle:Pedido')->find($id);
        $producto = $em->getRepository('ReconverpackBundle:ProductoPedido')->find($id_producto);

        $orden = $em->getRepository('ReconverpackBundle:OrdenProduccion')->findOneBy(array('pedido' => $pedido, 'productoPedido' => $producto));

        if ($orden) {
            //$orden->setEstado('cancelado');
            //$orden->setCantidadProducir(0);
            //$orden->setFechaTerminado(new \DateTime("now"));
            $em->remove($orden);
        }
        $em->remove($producto);
        $em->flush();
        return $this->redirectToRoute('pedido_edit', array('id' => $pedido->getId()));
    }

    /**
     * Deletes a pedido entity.
     *
     */
    public function deleteAction(Request $request, Pedido $pedido) {
        $form = $this->createDeleteForm($pedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pedido);
            $em->flush();
        }

        return $this->redirectToRoute('pedido_index');
    }

    /**
     * Creates a form to delete a pedido entity.
     *
     * @param Pedido $pedido The pedido entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Pedido $pedido) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('pedido_delete', array('id' => $pedido->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
