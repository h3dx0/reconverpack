<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrdenProduccion
 *
 * @ORM\Table(name="orden_produccion")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\OrdenProduccionRepository")
 */
class OrdenProduccion {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="folio", type="string", length=255)
     */
    private $folio;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidadProducir", type="integer")
     */
    private $cantidadProducir;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255)
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaSolicitado", type="date")
     */
    private $fechaSolicitado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaTerminado", type="date", nullable=true)
     */
    private $fechaTerminado;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidadProdTerminado", type="integer")
     * @Assert\Expression(expression="value <= this.getCantidadProducir()", message="La cantidad de producto terminado no puede ser mayor que la cantidad a producir.")
     */
    private $cantidadProdTerminado;

    /**
     * @ORM\ManyToOne(targetEntity="Pedido", inversedBy="ordenesProduccion")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     */
    private $pedido;

    /**
     * @ORM\OneToOne(targetEntity="ProductoPedido")
     * @ORM\JoinColumn(name="productoPedido_id", referencedColumnName="id")
     */
    private $productoPedido;

    public function __construct() {
        $this->estado = "Recibido";
        $this->folio = mt_rand();
    }

    public function __toString() {
        return $this->folio;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set folio
     *
     * @param string $folio
     * @return OrdenProduccion
     */
    public function setFolio($folio) {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get folio
     *
     * @return string
     */
    public function getFolio() {
        return $this->folio;
    }

    /**
     * Set cantidadProducir
     *
     * @param integer $cantidadProducir
     * @return OrdenProduccion
     */
    public function setCantidadProducir($cantidadProducir) {
        $this->cantidadProducir = $cantidadProducir;

        return $this;
    }

    /**
     * Get cantidadProducir
     *
     * @return integer
     */
    public function getCantidadProducir() {
        return $this->cantidadProducir;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return OrdenProduccion
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set cantidadProdTerminado
     *
     * @param integer $cantidadProdTerminado
     * @return OrdenProduccion
     */
    public function setCantidadProdTerminado($cantidadProdTerminado) {
        $this->cantidadProdTerminado = $cantidadProdTerminado;

        return $this;
    }

    /**
     * Get cantidadProdTerminado
     *
     * @return integer
     */
    public function getCantidadProdTerminado() {
        return $this->cantidadProdTerminado;
    }

    /**
     * Set pedido
     *
     * @param \ReconverpackBundle\Entity\Pedido $pedido
     * @return OrdenProduccion
     */
    public function setPedido(\ReconverpackBundle\Entity\Pedido $pedido = null) {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \ReconverpackBundle\Entity\Pedido
     */
    public function getPedido() {
        return $this->pedido;
    }

    /**
     * Set productoPedido
     *
     * @param \ReconverpackBundle\Entity\ProductoPedido $productoPedido
     * @return OrdenProduccion
     */
    public function setProductoPedido(\ReconverpackBundle\Entity\ProductoPedido $productoPedido = null) {
        $this->productoPedido = $productoPedido;

        return $this;
    }

    /**
     * Get productoPedido
     *
     * @return \ReconverpackBundle\Entity\ProductoPedido
     */
    public function getProductoPedido() {
        return $this->productoPedido;
    }

    /**
     * Set fechaSolicitado
     *
     * @param \DateTime $fechaSolicitado
     * @return OrdenProduccion
     */
    public function setFechaSolicitado($fechaSolicitado) {
        $this->fechaSolicitado = $fechaSolicitado;

        return $this;
    }

    /**
     * Get fechaSolicitado
     *
     * @return \DateTime
     */
    public function getFechaSolicitado() {
        return $this->fechaSolicitado;
    }

    /**
     * Set fechaTerminado
     *
     * @param \DateTime $fechaTerminado
     * @return OrdenProduccion
     */
    public function setFechaTerminado($fechaTerminado) {
        $this->fechaTerminado = $fechaTerminado;

        return $this;
    }

    /**
     * Get fechaTerminado
     *
     * @return \DateTime
     */
    public function getFechaTerminado() {
        return $this->fechaTerminado;
    }

}
