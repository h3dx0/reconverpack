<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\Cliente;
use ReconverpackBundle\Entity\Cotizacion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cotizacion controller.
 *
 */
class CotizacionController extends Controller
{
    /**
     * Lists all cotizacion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cotizacions = $em->getRepository('ReconverpackBundle:Cotizacion')->findBy(array(),array('id'=>'DESC'));

        return $this->render('cotizacion/index.html.twig', array(
            'cotizacions' => $cotizacions,
        ));
    }

    /**
     * Creates a new cotizacion entity.
     *
     */
    public function newAction(Request $request)
    {
        $cotizacion = new Cotizacion();
        $form = $this->createForm('ReconverpackBundle\Form\CotizacionType', $cotizacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cotizacion);
            $date=date_create();
            $nombreFichero = date_timestamp_get($date);
            $nombreCliente = $cotizacion->getNombreCliente();
            $cliente = new Cliente();
            $cliente->setRazonSocial($nombreCliente);
            $cliente->setEmail($cotizacion->getEmailCliente());
            $cliente->setTelefono($cotizacion->getTelefonoCliente());
            $cliente->setRFC($nombreFichero);
            $em->persist($cliente);
            $em->flush();

            $url = $this->get('kernel')->getRootDir() . '/../web/clientes/cotizacion/'.$nombreFichero.'.pdf';
            $html = $this->renderView('ReconverpackBundle:Default:cotizacion_pdf.html.twig', array(
                'cotizacion' => $cotizacion,
            ));
            $this->get('knp_snappy.pdf')->generateFromHtml($html,$url);
            /*enviar email*/
            $message = \Swift_Message::newInstance()
                ->setSubject('Cotización desde Reconverpack')
                ->setFrom('reddiversidadstats@gmail.com')
                ->setTo($cotizacion->getEmailCliente())
                ->setBody($this->renderView('ReconverpackBundle:Default:email_cotizacion.txt.twig', array('cliente'=>$nombreCliente)

                ));
            $message->attach(\Swift_Attachment::fromPath($url));
            $this->get('mailer')->send($message);
            return $this->redirectToRoute('cotizacion_show');
        }

        return $this->render('cotizacion/new.html.twig', array(
            'cotizacion' => $cotizacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cotizacion entity.
     *
     */
    public function showAction(Request $request)
    {

        return $this->render('cotizacion/show.html.twig');
    }

    /**
     * Displays a form to edit an existing cotizacion entity.
     *
     */
    public function editAction(Request $request, Cotizacion $cotizacion)
    {
        $deleteForm = $this->createDeleteForm($cotizacion);
        $editForm = $this->createForm('ReconverpackBundle\Form\CotizacionType', $cotizacion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cotizacion_edit', array('id' => $cotizacion->getId()));
        }

        return $this->render('cotizacion/edit.html.twig', array(
            'cotizacion' => $cotizacion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cotizacion entity.
     *
     */
    public function deleteAction(Request $request, Cotizacion $cotizacion)
    {
        $form = $this->createDeleteForm($cotizacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cotizacion);
            $em->flush();
        }

        return $this->redirectToRoute('cotizacion_index');
    }

    /**
     * Creates a form to delete a cotizacion entity.
     *
     * @param Cotizacion $cotizacion The cotizacion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cotizacion $cotizacion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cotizacion_delete', array('id' => $cotizacion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
