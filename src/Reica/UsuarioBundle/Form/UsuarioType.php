<?php

namespace Reica\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                //->add('persona',)
                ->add('username',null,array('label'=>'Usuario'))
                ->add('nombre',null,array('label'=>'Nombre'))
                ->add('apellidoPaterno',null,array('label'=>'Apellido Paterno'))
                ->add('apellidoMaterno',null,array('label'=>'Apellido Materno'))
                ->add('email',null,array('label'=>'Correo'))
                ->add('password',null,array('label'=>'Contraseña'))
                ->add('grupos')
                ->add('rolesPropios', null, array('required' => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Reica\UsuarioBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'Reica_usuariobundle_usuario';
    }

}
