<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lote
 *
 * @ORM\Table(name="lote")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\LoteRepository")
 */
class Lote
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numLote", type="string", length=255)
     */
    private $numLote;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoMaterial", type="string", length=255, nullable=true)
     */
    private $tipoMaterial;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreProveedor", type="string", length=255)
     */
    private $nombreProveedor;

    /**
     * @var string
     *
     * @ORM\Column(name="medidas", type="string", length=255, nullable=true)
     */
    private $medidas;

    /**
     * @var bool
     *
     * @ORM\Column(name="poli", type="string", length=255, nullable=true)
     */
    private $tienePoli;

    /**
     * @var string
     *
     * @ORM\Column(name="puntos", type="string", length=255, nullable=true)
     */
    private $puntos;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="text")
     */
    private $observacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaAlta", type="date", nullable=true)
     */
    private $fechaAlta;

    public function __toString()
    {
        return $this->getNumLote();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numLote
     *
     * @param string $numLote
     *
     * @return Lote
     */
    public function setNumLote($numLote)
    {
        $this->numLote = $numLote;

        return $this;
    }

    /**
     * Get numLote
     *
     * @return string
     */
    public function getNumLote()
    {
        return $this->numLote;
    }

    /**
     * Set tipoMaterial
     *
     * @param string $tipoMaterial
     *
     * @return Lote
     */
    public function setTipoMaterial($tipoMaterial)
    {
        $this->tipoMaterial = $tipoMaterial;

        return $this;
    }

    /**
     * Get tipoMaterial
     *
     * @return string
     */
    public function getTipoMaterial()
    {
        return $this->tipoMaterial;
    }

    /**
     * Set nombreProveedor
     *
     * @param string $nombreProveedor
     *
     * @return Lote
     */
    public function setNombreProveedor($nombreProveedor)
    {
        $this->nombreProveedor = $nombreProveedor;

        return $this;
    }

    /**
     * Get nombreProveedor
     *
     * @return string
     */
    public function getNombreProveedor()
    {
        return $this->nombreProveedor;
    }

    /**
     * Set medidas
     *
     * @param string $medidas
     *
     * @return Lote
     */
    public function setMedidas($medidas)
    {
        $this->medidas = $medidas;

        return $this;
    }

    /**
     * Get medidas
     *
     * @return string
     */
    public function getMedidas()
    {
        return $this->medidas;
    }

    /**
     * Set tienePoli
     *
     * @param boolean $tienePoli
     *
     * @return Lote
     */
    public function setTienePoli($tienePoli)
    {
        $this->tienePoli = $tienePoli;

        return $this;
    }

    /**
     * Get tienePoli
     *
     * @return bool
     */
    public function getTienePoli()
    {
        return $this->tienePoli;
    }

    /**
     * Set puntos
     *
     * @param string $puntos
     *
     * @return Lote
     */
    public function setPuntos($puntos)
    {
        $this->puntos = $puntos;

        return $this;
    }

    /**
     * Get puntos
     *
     * @return string
     */
    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return Lote
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     *
     * @return Lote
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }
}
