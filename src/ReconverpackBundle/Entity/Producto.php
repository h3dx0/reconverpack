<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Producto
 *
 * @ORM\Table(name="producto")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\ProductoRepository")
 */
class Producto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="clave", type="string", length=255)
     */
    private $clave;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidadEnInventario", type="integer", nullable=true)
     */
    private $cantidadEnInventario;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidadCapas", type="float", length=255)
     */
    private $cantidadCapas;

    /**
     * @var string
     *
     * @ORM\Column(name="medida", type="string", length=255)
     */
    private $medida;
    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="float", length=255)
     */
    private $precio;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria")
     *@ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    private $categoria;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaAlta", type="date", nullable=true)
     */
    private $fechaAlta;
    //
    // /**
    //  * @ORM\ManyToMany(targetEntity="Material",cascade={"persist"})
    //  */
    // private $material;

    public function __construct() {
        //$this->material = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString(){
        return $this->nombre;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Producto
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave
     *
     * @return string
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set unidad
     *
     * @param string $unidad
     * @return Producto
     */
    public function setUnidad($unidad)
    {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return string
     */
    public function getUnidad()
    {
        return $this->unidad;
    }

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     * @return Producto
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }
    /**
     * Set cantidadEnInventario
     *
     * @param integer $cantidadEnInventario
     * @return Producto
     */
    public function setCantidadEnInventario($cantidadEnInventario)
    {
        $this->cantidadEnInventario = $cantidadEnInventario;

        return $this;
    }

    /**
     * Get cantidadEnInventario
     *
     * @return integer
     */
    public function getCantidadEnInventario()
    {
        return $this->cantidadEnInventario;
    }

    /**
     * Add material
     *
     * @param \ReconverpackBundle\Entity\Material $material
     * @return Producto
     */
    // public function addMaterial(\ReconverpackBundle\Entity\Material $material)
    // {
    //     $this->material[] = $material;
    //
    //     return $this;
    // }

    // /**
    //  * Remove material
    //  *
    //  * @param \ReconverpackBundle\Entity\Material $material
    //  */
    // public function removeMaterial(\ReconverpackBundle\Entity\Material $material)
    // {
    //     $this->material->removeElement($material);
    // }
    //
    // /**
    //  * Get material
    //  *
    //  * @return \Doctrine\Common\Collections\Collection
    //  */
    // public function getMaterial()
    // {
    //     return $this->material;
    // }

    /**
     * Set color
     *
     * @param string $color
     * @return Producto
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set grosor
     *
     * @param \float $grosor
     * @return Producto
     */
    public function setGrosor($grosor)
    {
        $this->grosor = $grosor;

        return $this;
    }

    /**
     * Get grosor
     *
     * @return \float
     */
    public function getGrosor()
    {
        return $this->grosor;
    }

       /**
     * Set precio
     *
     * @param \float $precio
     * @return Producto
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return \float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set categoria
     *
     * @param \ReconverpackBundle\Entity\Categoria $categoria
     *
     * @return Producto
     */
    public function setCategoria(\ReconverpackBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \ReconverpackBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set cantidadCapas
     *
     * @param float $cantidadCapas
     *
     * @return Producto
     */
    public function setCantidadCapas($cantidadCapas)
    {
        $this->cantidadCapas = $cantidadCapas;

        return $this;
    }

    /**
     * Get cantidadCapas
     *
     * @return float
     */
    public function getCantidadCapas()
    {
        return $this->cantidadCapas;
    }

    /**
     * Set medida
     *
     * @param string $medida
     *
     * @return Producto
     */
    public function setMedida($medida)
    {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get medida
     *
     * @return string
     */
    public function getMedida()
    {
        return $this->medida;
    }
}
