<?php

namespace ReconverpackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use ReconverpackBundle\Form\ProductoPedidoType;
class PedidoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
          ->add('estado',ChoiceType::class, array(
              'required'=>false,
              'choices'  => array(
                  'recibido' => 'Recibido',
                  'produccion' => 'Produccion',
                  'enviado' => 'Enviado',
                  'cancelado' => 'Cancelado',
              )))

          ->add('productoCliente', CollectionType::class, array(
              'entry_type' => ProductoPedidoType::class,
              'allow_add'  => true,
          ))
      ->add('fechaEntrega')
//      ->add('numeroBobina')
      ->add('cliente')
      ->add('formaEnvio')
      ->add('descuento')
      ->add('descripcion')
    ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ReconverpackBundle\Entity\Pedido'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reconverpackbundle_pedido';
    }


}
