<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\SubCategoria;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Subcategorium controller.
 *
 */
class SubCategoriaController extends Controller
{
    /**
     * Lists all subCategorium entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $subCategorias = $em->getRepository('ReconverpackBundle:SubCategoria')->findAll();

        return $this->render('subcategoria/index.html.twig', array(
            'subCategorias' => $subCategorias,
        ));
    }

    /**
     * Creates a new subCategorium entity.
     *
     */
    public function newAction(Request $request)
    {
        $subCategorium = new SubCategoria();
        $form = $this->createForm('ReconverpackBundle\Form\SubCategoriaType', $subCategorium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($subCategorium);
            $em->flush();

            return $this->redirectToRoute('subcategoria_show', array('id' => $subCategorium->getId()));
        }

        return $this->render('subcategoria/new.html.twig', array(
            'subCategorium' => $subCategorium,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a subCategorium entity.
     *
     */
    public function showAction(SubCategoria $subCategorium)
    {
        $deleteForm = $this->createDeleteForm($subCategorium);

        return $this->render('subcategoria/show.html.twig', array(
            'subCategorium' => $subCategorium,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing subCategorium entity.
     *
     */
    public function editAction(Request $request, SubCategoria $subCategorium)
    {
        $deleteForm = $this->createDeleteForm($subCategorium);
        $editForm = $this->createForm('ReconverpackBundle\Form\SubCategoriaType', $subCategorium);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('subcategoria_edit', array('id' => $subCategorium->getId()));
        }

        return $this->render('subcategoria/edit.html.twig', array(
            'subCategorium' => $subCategorium,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a subCategorium entity.
     *
     */
    public function deleteAction(Request $request, SubCategoria $subCategorium)
    {
        $form = $this->createDeleteForm($subCategorium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($subCategorium);
            $em->flush();
        }

        return $this->redirectToRoute('subcategoria_index');
    }

    /**
     * Creates a form to delete a subCategorium entity.
     *
     * @param SubCategoria $subCategorium The subCategorium entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SubCategoria $subCategorium)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('subcategoria_delete', array('id' => $subCategorium->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
