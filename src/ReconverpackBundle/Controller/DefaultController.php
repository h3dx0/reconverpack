<?php

namespace ReconverpackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {

      // $em = $this->getDoctrine()->getManager();
      // $ordenesProduccion = $em->getRepository('ReconverpackBundle:OrdenProduccion')
      //   ->findBy(array('estado'=>'terminado'));
      //
      // $listadoProductos = array();
      // foreach ($ordenesProduccion as $key => $ordenProd) {
      //   array_push($listadoProductos,$ordenProd->getProductoPedido()->getProductoCliente()->getProducto()->getId());
      // }
      // $nuevoListado = array_count_values($listadoProductos);
      //
      // arsort($nuevoListado);
      // $llaves = array();
      // foreach ($nuevoListado as $key => $value) {
      //   array_push($llaves, $key);
      // }
      // $qb = $em->createQueryBuilder();
      // $qb->add('select', 'p')
      //    ->add('from', 'ReconverpackBundle:Producto p')
      //    ->add('where', $qb->expr()->in('p.id', $llaves));
      // $query = $qb->getQuery();
      //   $productos = $query->getResult();
        return $this->render('ReconverpackBundle:Default:index.html.twig', array(
            'productos'=>array()
        ));
    }
}
