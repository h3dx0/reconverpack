<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\Bobina;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Bobina controller.
 *
 */
class BobinaController extends Controller
{
    /**
     * Lists all bobina entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $bobinas = $em->getRepository('ReconverpackBundle:Bobina')->findAll();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $bobinas, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 15/* limit per page */
        );

        return $this->render('bobina/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new bobina entity.
     *
     */
    public function newAction(Request $request)
    {
        $bobina = new Bobina();
        $form = $this->createForm('ReconverpackBundle\Form\BobinaType', $bobina);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bobina);
            $em->flush();

            return $this->redirectToRoute('bobina_index');
        }

        return $this->render('bobina/new.html.twig', array(
            'bobina' => $bobina,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a bobina entity.
     *
     */
    public function showAction(Bobina $bobina)
    {
        $deleteForm = $this->createDeleteForm($bobina);

        return $this->render('bobina/show.html.twig', array(
            'bobina' => $bobina,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing bobina entity.
     *
     */
    public function editAction(Request $request, Bobina $bobina)
    {
        $deleteForm = $this->createDeleteForm($bobina);
        $editForm = $this->createForm('ReconverpackBundle\Form\BobinaEditarType', $bobina);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bobina_index');
        }

        return $this->render('bobina/edit.html.twig', array(
            'bobina' => $bobina,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a bobina entity.
     *
     */
    public function deleteAction(Request $request, Bobina $bobina)
    {
        $form = $this->createDeleteForm($bobina);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bobina);
            $em->flush();
        }

        return $this->redirectToRoute('bobina_index');
    }

    /**
     * Creates a form to delete a bobina entity.
     *
     * @param Bobina $bobina The bobina entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bobina $bobina)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bobina_delete', array('id' => $bobina->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
