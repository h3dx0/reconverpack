<?php

namespace ReconverpackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class CotizacionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreCliente')
            ->add('emailCliente')
            ->add('telefonoCliente')
            ->add('producto',CollectionType::class, array(
                'entry_type' => ProductoCotizacionType::class,
                'allow_add'  => true,
            ))
            ->add('descuento')
            ->add('total', HiddenType::class)
            ->add('observacionGeneral');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ReconverpackBundle\Entity\Cotizacion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reconverpackbundle_cotizacion';
    }


}
