<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\Insumos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ReconverpackBundle\Form\InsumosType;
/**
 * Insumo controller.
 *
 */
class InsumosController extends Controller
{
    /**
     * Lists all insumo entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $insumos = $em->getRepository('ReconverpackBundle:Insumos')->findAll();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $insumos, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 15/* limit per page */
        );

        return $this->render('insumos/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new insumo entity.
     *
     */
    public function newAction(Request $request)
    {
        $insumo = new Insumos();
        $form = $this->createForm('ReconverpackBundle\Form\InsumosType', $insumo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($insumo);
            $em->flush();

            return $this->redirectToRoute('insumos_index');
        }

        return $this->render('insumos/new.html.twig', array(
            'insumo' => $insumo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a insumo entity.
     *
     */
    public function showAction(Insumos $insumo)
    {
        $deleteForm = $this->createDeleteForm($insumo);

        return $this->render('insumos/show.html.twig', array(
            'insumo' => $insumo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing insumo entity.
     *
     */
    public function editAction(Request $request, Insumos $insumo)
    {
        $deleteForm = $this->createDeleteForm($insumo);
        $editForm = $this->createForm('ReconverpackBundle\Form\InsumosEditType', $insumo);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('insumos_index');
        }

        return $this->render('insumos/edit.html.twig', array(
            'insumo' => $insumo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a insumo entity.
     *
     */
    public function deleteAction(Request $request, Insumos $insumo)
    {
        $form = $this->createDeleteForm($insumo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($insumo);
            $em->flush();
        }

        return $this->redirectToRoute('insumos_index');
    }

    /**
     * Creates a form to delete a insumo entity.
     *
     * @param Insumos $insumo The insumo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Insumos $insumo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('insumos_delete', array('id' => $insumo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
