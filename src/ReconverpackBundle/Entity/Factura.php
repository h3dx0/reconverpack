<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints\Date;
/**
 * Factura
 *
 * @ORM\Table(name="factura")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\FacturaRepository")
 * @Vich\Uploadable
 */
class Factura
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="folio", type="string", length=255)
     */
    private $folio;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @Vich\UploadableField(mapping="clientes_factura", fileNameProperty="nombre", size="fileSize")
     */
    private $fichero;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="fechaAlta", type="date", nullable=true)
    */
    private $fechaAlta;

    /**
    * @ORM\Column(type="integer")
    *
    * @var integer
    */
    private $fileSize;

    /**
    * @ORM\ManyToOne(targetEntity="Cliente", inversedBy="facturas")
    */
    private $cliente;

    /**
    * @ORM\ManyToOne(targetEntity="Pedido", inversedBy="facturas")
    */
    private $pedido;
    /**
    * @ORM\Column(type="datetime")
    *
    * @var \DateTime
    */
    private $updatedAt;


        public function __construct()
        {
            $this->fileSize = 0.2;
            $this->fechaAlta = new \DateTime('now');
        }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set fichero
     *
     * @param string $fichero
     * @return Factura
     */
    public function setFichero(File $fichero = null)
    {
        $this->fichero = $fichero;
        if ($fichero) {
          // It is required that at least one field changes if you are using doctrine
          // otherwise the event listeners won't be called and the file is lost
          $this->updatedAt = new \DateTimeImmutable();
        }
        return $this;
    }

    /**
     * Get fichero
     *
     * @return string
     */
    public function getFichero()
    {
        return $this->fichero;
    }

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     * @return Factura
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set fileSize
     *
     * @param integer $fileSize
     * @return Factura
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * Get fileSize
     *
     * @return integer
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Factura
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set cliente
     *
     * @param \ReconverpackBundle\Entity\Cliente $cliente
     * @return Factura
     */
    public function setCliente(\ReconverpackBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \ReconverpackBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set pedido
     *
     * @param \ReconverpackBundle\Entity\Pedido $pedido
     * @return Factura
     */
    public function setPedido(\ReconverpackBundle\Entity\Pedido $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \ReconverpackBundle\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set folio
     *
     * @param string $folio
     * @return Factura
     */
    public function setFolio($folio)
    {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get folio
     *
     * @return string 
     */
    public function getFolio()
    {
        return $this->folio;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Factura
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}
