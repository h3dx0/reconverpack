<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\Categoria;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Categoria controller.
 *
 */
class CategoriaController extends Controller {

    /**
     * Lists all Categoria entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('ReconverpackBundle:Categoria')->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $categorias, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 15/* limit per page */
        );

        return $this->render('categoria/index.html.twig', array(
                    'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new Categoria entity.
     *
     */
    public function newAction(Request $request) {
        $Categoria = new Categoria();
        $form = $this->createForm('ReconverpackBundle\Form\CategoriaType', $Categoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Categoria);
            $em->flush();

            return $this->redirectToRoute('categoria_index');
        }

        return $this->render('categoria/new.html.twig', array(
                    'Categoria' => $Categoria,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Categoria entity.
     *
     */
    public function showAction(Categoria $Categoria) {
        $deleteForm = $this->createDeleteForm($Categoria);

        return $this->render('categoria/show.html.twig', array(
                    'Categoria' => $Categoria,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Categoria entity.
     *
     */
    public function editAction(Request $request, Categoria $Categoria) {
        $deleteForm = $this->createDeleteForm($Categoria);
        $editForm = $this->createForm('ReconverpackBundle\Form\CategoriaType', $Categoria);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('categoria_index');
        }

        return $this->render('categoria/edit.html.twig', array(
                    'Categoria' => $Categoria,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Categoria entity.
     *
     */
    public function deleteAction(Request $request, Categoria $Categoria) {
        $form = $this->createDeleteForm($Categoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($Categoria);
            $em->flush();
        }

        return $this->redirectToRoute('categoria_index');
    }

    /**
     * Creates a form to delete a Categoria entity.
     *
     * @param Categoria $Categoria The Categoria entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Categoria $Categoria) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('categoria_delete', array('id' => $Categoria->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
