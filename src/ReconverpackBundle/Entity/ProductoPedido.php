<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductoPedido
 *
 * @ORM\Table(name="producto_pedido")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\ProductoPedidoRepository")
 */
class ProductoPedido
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidad", type="float")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="Producto")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    private $producto;

    public function __toString(){
      return $this->producto."-".$this->cantidad;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return ProductoPedido
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }


    /**
     * Set producto
     *
     * @param \ReconverpackBundle\Entity\Producto $producto
     *
     * @return ProductoPedido
     */
    public function setProducto(\ReconverpackBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \ReconverpackBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }
}
