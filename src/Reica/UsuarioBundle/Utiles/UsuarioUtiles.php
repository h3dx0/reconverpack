<?php

/**
 * Created by JetBrains PhpStorm.
 * User: robe
 * Date: 29/07/13
 * Time: 11:18
 * To change this template use File | Settings | File Templates.
 */

namespace Reica\UsuarioBundle\Utiles;

use Reica\PersonalBundle\Entity\Persona;
use Reica\UsuarioBundle\Entity\Usuario;
use Reica\UsuarioBundle\Entity\Grupo;

class UsuarioUtiles {

    private $persona;
    private $em;
    public $service_container;

    public function __construct($service_container) {
        $this->service_container = $service_container;
        $this->em = $this->service_container->get('doctrine')->getManager();
    }

    public function addUsuario($persona,$usuario, $grupo) {

        $usuarioObj = new Usuario();
        $grupo2 = $this->em->getRepository('UsuarioBundle:Grupo')->findOneBy(array('nombre' => 'ROLE_USER'));
        $encoder = $this->service_container->get('security.encoder_factory')->getEncoder($usuarioObj);
        
        $passwordEncode = $encoder->encodePassword(
                $usuario->getPassword(), $usuarioObj->getSalt()
        );
       // print_r($passwordEncode);exit;
        $usuarioObj->setUsername(strtolower($usuario->getUsername()));
        $usuarioObj->setEmail($usuario->getEmail());
        $usuarioObj->setPassword($passwordEncode);
        $usuarioObj->setEnabled(true);
        $usuarioObj->setPersona($persona);
        $usuarioObj->addGrupo($grupo);
        $usuarioObj->addGrupo($grupo2);
        // falta mandar el correo al ususario
        //$this->em->persist($usuario);
        return $usuarioObj;
    }

    public function editPassword($password) {
        $usuarioObj = new Usuario();

        $encoder = $this->service_container->get('security.encoder_factory')->getEncoder($usuarioObj);
        $passwordEncode = $encoder->encodePassword(
                $password, $usuarioObj->getSalt()
        );
         $usuarioObj->setPassword($passwordEncode);
        $usuarioObj->setPassword($passwordEncode);
        return $usuarioObj;
    }

}
