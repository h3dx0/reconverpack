<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\Lote;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Lote controller.
 *
 */
class LoteController extends Controller
{
    /**
     * Lists all lote entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $lotes = $em->getRepository('ReconverpackBundle:Lote')->findAll();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $lotes, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 15/* limit per page */
        );


        return $this->render('lote/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new lote entity.
     *
     */
    public function newAction(Request $request)
    {
        $lote = new Lote();
        $form = $this->createForm('ReconverpackBundle\Form\LoteType', $lote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($lote);
            $em->flush();

            return $this->redirectToRoute('lote_index');
        }

        return $this->render('lote/new.html.twig', array(
            'lote' => $lote,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a lote entity.
     *
     */
    public function showAction(Lote $lote)
    {
        $deleteForm = $this->createDeleteForm($lote);

        return $this->render('lote/show.html.twig', array(
            'lote' => $lote,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing lote entity.
     *
     */
    public function editAction(Request $request, Lote $lote)
    {
        $deleteForm = $this->createDeleteForm($lote);
        $editForm = $this->createForm('ReconverpackBundle\Form\LoteType', $lote);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lote_index');
        }

        return $this->render('lote/edit.html.twig', array(
            'lote' => $lote,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a lote entity.
     *
     */
    public function deleteAction(Request $request, Lote $lote)
    {
        $form = $this->createDeleteForm($lote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($lote);
            $em->flush();
        }

        return $this->redirectToRoute('lote_index');
    }

    /**
     * Creates a form to delete a lote entity.
     *
     * @param Lote $lote The lote entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Lote $lote)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lote_delete', array('id' => $lote->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
