<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\Merma;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Merma controller.
 *
 */
class MermaController extends Controller
{
    /**
     * Lists all merma entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $mermas = $em->getRepository('ReconverpackBundle:Merma')->findAll();

        return $this->render('merma/index.html.twig', array(
            'mermas' => $mermas,
        ));
    }

    /**
     * Creates a new merma entity.
     *
     */
    public function newAction(Request $request)
    {
        $merma = new Merma();
        $form = $this->createForm('ReconverpackBundle\Form\MermaType', $merma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($merma);
            $em->flush();

            return $this->redirectToRoute('merma_index');
        }

        return $this->render('merma/new.html.twig', array(
            'merma' => $merma,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a merma entity.
     *
     */
    public function showAction(Merma $merma)
    {
        $deleteForm = $this->createDeleteForm($merma);

        return $this->render('merma/show.html.twig', array(
            'merma' => $merma,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing merma entity.
     *
     */
    public function editAction(Request $request, Merma $merma)
    {
        $deleteForm = $this->createDeleteForm($merma);
        $editForm = $this->createForm('ReconverpackBundle\Form\MermaType', $merma);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('merma_edit', array('id' => $merma->getId()));
        }

        return $this->render('merma/edit.html.twig', array(
            'merma' => $merma,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a merma entity.
     *
     */
    public function deleteAction(Request $request, Merma $merma)
    {
        $form = $this->createDeleteForm($merma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($merma);
            $em->flush();
        }

        return $this->redirectToRoute('merma_index');
    }

    /**
     * Creates a form to delete a merma entity.
     *
     * @param Merma $merma The merma entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Merma $merma)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('merma_delete', array('id' => $merma->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
