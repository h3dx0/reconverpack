<?php

namespace ReconverpackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class LoteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numLote')
            ->add('tipoMaterial')
            ->add('nombreProveedor')
            ->add('medidas')
            ->add('tienePoli', ChoiceType::class, array(
                'required'=>false,
                'choices'  => array(
                    'tiene' => 'Tiene por 1 lado',
                    'tieneoslados' => 'Tiene por los 2 lados',
                    'notiene' => 'No Tiene',
                )))
            ->add('puntos')
            ->add('fechaAlta')
            ->add('observacion');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ReconverpackBundle\Entity\Lote'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reconverpackbundle_lote';
    }


}
