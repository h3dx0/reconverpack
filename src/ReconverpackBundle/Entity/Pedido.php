<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pedido
 *
 * @ORM\Table(name="pedido")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\PedidoRepository")
 */
class Pedido
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="folio", type="string", length=255)
     */
    private $folio;
    /**
     * @var string
     *
     * @ORM\Column(name="formaEnvio", type="string", length=255, nullable=true)
     */
    private $formaEnvio;
    /**
     * @var string
     *
     * @ORM\Column(name="descuento", type="string", length=255, nullable=true)
     */
    private $descuento;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", length=800, nullable=true)
     */
    private $descripcion;
    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255,nullable=true)
     */
    private $estado;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaRegistro", type="date",nullable=true)
     */
    private $fechaRegistro;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaEnvio", type="date",nullable=true)
     */
    private $fechaEnvio;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaEntrega", type="date", nullable=true)
     */
    private $fechaEntrega;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaTerminadoProduccion", type="date", nullable=true)
     */
    private $fechaTerminadoProduccion;

    /**
     * @ORM\ManyToMany(targetEntity="ProductoPedido",cascade={"persist"})
     */
    private $productoCliente;

      /**
       * @ORM\ManyToOne(targetEntity="Cliente", inversedBy="pedidos")
       */
      private $cliente;

    public function __construct() {
        $this->productoCliente = new \Doctrine\Common\Collections\ArrayCollection();
        $this->estado = "recibido";

        $this->folio = mt_rand();
//        $this->fechaEntrada = new \DateTime('now');

    }
    public function __toString(){
      return $this->folio;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Pedido
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }


    /**
     * Set precioCliente
     *
     * @param \ReconverpackBundle\Entity\PrecioProdCliente $precioCliente
     * @return Pedido
     */
    public function setPrecioCliente(\ReconverpackBundle\Entity\PrecioProdCliente $precioCliente = null)
    {
        $this->precioCliente = $precioCliente;

        return $this;
    }
    /**
     * Set folio
     *
     * @param string $folio
     * @return Pedido
     */
    public function setFolio($folio)
    {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get folio
     *
     * @return string
     */
    public function getFolio()
    {
        return $this->folio;
    }

    /**
     * Set numeroBobina
     *
     * @param string $numeroBobina
     * @return Pedido
     */
    public function setNumeroBobina($numeroBobina)
    {
        $this->numeroBobina = $numeroBobina;

        return $this;
    }

    /**
     * Get numeroBobina
     *
     * @return string
     */
    public function getNumeroBobina()
    {
        return $this->numeroBobina;
    }

    /**
     * Set fechaEntrega
     *
     * @param \DateTime $fechaEntrega
     * @return Pedido
     */
    public function setFechaEntrega($fechaEntrega)
    {
        $this->fechaEntrega = $fechaEntrega;

        return $this;
    }

    /**
     * Get fechaEntrega
     *
     * @return \DateTime
     */
    public function getFechaEntrega()
    {
        return $this->fechaEntrega;
    }


    /**
     * Set cliente
     *
     * @param \ReconverpackBundle\Entity\Cliente $cliente
     * @return Pedido
     */
    public function setCliente(\ReconverpackBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \ReconverpackBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set fechaRegistro
     *
     * @param \DateTime $fechaRegistro
     *
     * @return Pedido
     */
    public function setFechaRegistro($fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    /**
     * Get fechaRegistro
     *
     * @return \DateTime
     */
    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    /**
     * Set fechaTerminadoProduccion
     *
     * @param \DateTime $fechaTerminadoProduccion
     *
     * @return Pedido
     */
    public function setFechaTerminadoProduccion($fechaTerminadoProduccion)
    {
        $this->fechaTerminadoProduccion = $fechaTerminadoProduccion;

        return $this;
    }

    /**
     * Get fechaTerminadoProduccion
     *
     * @return \DateTime
     */
    public function getFechaTerminadoProduccion()
    {
        return $this->fechaTerminadoProduccion;
    }

    /**
     * Set fechaEnvio
     *
     * @param \DateTime $fechaEnvio
     *
     * @return Pedido
     */
    public function setFechaEnvio($fechaEnvio)
    {
        $this->fechaEnvio = $fechaEnvio;

        return $this;
    }

    /**
     * Get fechaEnvio
     *
     * @return \DateTime
     */
    public function getFechaEnvio()
    {
        return $this->fechaEnvio;
    }

    /**
     * Add cotizacion
     *
     * @param \ReconverpackBundle\Entity\Cotizacion $cotizacion
     *
     * @return Pedido
     */
    public function addCotizacion(\ReconverpackBundle\Entity\Cotizacion $cotizacion)
    {
        $this->cotizacion[] = $cotizacion;

        return $this;
    }

    /**
     * Remove cotizacion
     *
     * @param \ReconverpackBundle\Entity\Cotizacion $cotizacion
     */
    public function removeCotizacion(\ReconverpackBundle\Entity\Cotizacion $cotizacion)
    {
        $this->cotizacion->removeElement($cotizacion);
    }

    /**
     * Get cotizacion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCotizacion()
    {
        return $this->cotizacion;
    }


    /**
     * Add productoCliente
     *
     * @param \ReconverpackBundle\Entity\ProductoPedido $productoCliente
     *
     * @return Pedido
     */
    public function addProductoCliente(\ReconverpackBundle\Entity\ProductoPedido $productoCliente)
    {
        $this->productoCliente[] = $productoCliente;

        return $this;
    }

    /**
     * Remove productoCliente
     *
     * @param \ReconverpackBundle\Entity\ProductoPedido $productoCliente
     */
    public function removeProductoCliente(\ReconverpackBundle\Entity\ProductoPedido $productoCliente)
    {
        $this->productoCliente->removeElement($productoCliente);
    }

    /**
     * Get productoCliente
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductoCliente()
    {
        return $this->productoCliente;
    }

    /**
     * Set formaEnvio
     *
     * @param string $formaEnvio
     *
     * @return Pedido
     */
    public function setFormaEnvio($formaEnvio)
    {
        $this->formaEnvio = $formaEnvio;

        return $this;
    }

    /**
     * Get formaEnvio
     *
     * @return string
     */
    public function getFormaEnvio()
    {
        return $this->formaEnvio;
    }

    /**
     * Set descuento
     *
     * @param string $descuento
     *
     * @return Pedido
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return string
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Pedido
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
