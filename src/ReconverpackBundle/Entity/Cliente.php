<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\ClienteRepository")
 */
class Cliente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="razonSocial", type="string", length=255)
     */
    private $razonSocial;

    /**
     * @var string
     *
     * @ORM\Column(name="RFC", type="string", length=255, unique=true)
     */
    private $rFC;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="telefono", type="string",length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="text", nullable=true)
     */
    private $direccion;

    /**
     * @ORM\ManyToMany(targetEntity="Producto",cascade={"persist"})
     */
    private $productos;
    
    
    /**
     * One Cliente has Many Pedidos.
     * @ORM\OneToMany(targetEntity="Pedido", mappedBy="cliente")
     */
    private $pedidos;

    public function __construct() {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString(){
        return $this->razonSocial;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     * @return Cliente
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set rFC
     *
     * @param string $rFC
     * @return Cliente
     */
    public function setRFC($rFC)
    {
        $this->rFC = $rFC;

        return $this;
    }

    /**
     * Get rFC
     *
     * @return string
     */
    public function getRFC()
    {
        return $this->rFC;
    }

    /**
     * Set telefono
     *
     * @param integer $telefono
     * @return Cliente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return integer
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Cliente
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Add produto
     *
     * @param \ReconverpackBundle\Entity\Producto $produto
     * @return Cliente
     */
    public function addProduto(\ReconverpackBundle\Entity\Producto $produto)
    {
        $this->produto[] = $produto;

        return $this;
    }

    /**
     * Remove produto
     *
     * @param \ReconverpackBundle\Entity\Producto $produto
     */
    public function removeProduto(\ReconverpackBundle\Entity\Producto $produto)
    {
        $this->produto->removeElement($produto);
    }

    /**
     * Get produto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * Add pedidos
     *
     * @param \ReconverpackBundle\Entity\Pedido $pedidos
     * @return Cliente
     */
    public function addPedidos(\ReconverpackBundle\Entity\Pedido $pedidos)
    {
        $this->pedidos[] = $pedidos;

        return $this;
    }

    /**
     * Remove pedido
     *
     * @param \ReconverpackBundle\Entity\Pedido $pedido
     */
    public function removePedido(\ReconverpackBundle\Entity\Pedido $pedido)
    {
        $this->productos->removeElement($pedido);
    }

    /**
     * Get pedidos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPedidos()
    {
        return $this->pedidos;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
}
