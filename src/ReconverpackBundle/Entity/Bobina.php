<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Bobina
 *
 * @ORM\Table(name="bobina")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\BobinaRepository")
 */
class Bobina {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=255)
     */
    private $numero;

    /**
     * @var float
     *
     * @ORM\Column(name="medida", type="float")
     * @Assert\Type(
     *      type="float",
     *      message="El valor {{ value }} no es una Medida válida."
     * )
     */
    private $medida;

    /**
     * @var float
     *
     * @ORM\Column(name="peso", type="float")
     * @Assert\Type(
     *      type="float",
     *      message="El valor {{ value }} no es un Peso válido."
     * )
     */
    private $peso;

    /**
     * @ORM\ManyToOne(targetEntity="Lote", inversedBy="bobinas")
     * @ORM\JoinColumn(name="lote_id", referencedColumnName="id")
     */
    private $lote;

    public function __toString() {
        return $this->getNumero() . "-" . $this->getLote();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return Bobina
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set medida
     *
     * @param float $medida
     * @return Bobina
     */
    public function setMedida($medida) {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get medida
     *
     * @return float
     */
    public function getMedida() {
        return $this->medida;
    }

    /**
     * Set peso
     *
     * @param float $peso
     * @return Bobina
     */
    public function setPeso($peso) {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return float
     */
    public function getPeso() {
        return $this->peso;
    }

    /**
     * Set lote
     *
     * @param \ReconverpackBundle\Entity\Lote $lote
     *
     * @return Bobina
     */
    public function setLote(\ReconverpackBundle\Entity\Lote $lote = null) {
        $this->lote = $lote;

        return $this;
    }

    /**
     * Get lote
     *
     * @return \ReconverpackBundle\Entity\Lote
     */
    public function getLote() {
        return $this->lote;
    }

}
