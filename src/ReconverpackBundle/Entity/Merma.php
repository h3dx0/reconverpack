<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Merma
 *
 * @ORM\Table(name="merma")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\MermaRepository")
 */
class Merma
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidad", type="string", length=255)
     */
    private $cantidad;
    /**
     * @ORM\ManyToOne(targetEntity="Bobina")
     *@ORM\JoinColumn(name="bobina_id", referencedColumnName="id")
     */
    private $bobina;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="fechaAlta", type="date", nullable=true)
    */
    private $fechaAlta;
    /**
     * @var string
     *
     * @ORM\Column(name="operador", type="string", length=255)
     */
    private $operador;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     * @return Merma
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

  

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     * @return Merma
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set bobina
     *
     * @param \ReconverpackBundle\Entity\Bobina $bobina
     * @return Merma
     */
    public function setBobina(\ReconverpackBundle\Entity\Bobina $bobina = null)
    {
        $this->bobina = $bobina;

        return $this;
    }

    /**
     * Get bobina
     *
     * @return \ReconverpackBundle\Entity\Bobina 
     */
    public function getBobina()
    {
        return $this->bobina;
    }

    /**
     * Set operador
     *
     * @param string $operador
     *
     * @return Merma
     */
    public function setOperador($operador)
    {
        $this->operador = $operador;

        return $this;
    }

    /**
     * Get operador
     *
     * @return string
     */
    public function getOperador()
    {
        return $this->operador;
    }
}
