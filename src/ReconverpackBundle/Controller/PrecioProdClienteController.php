<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\PrecioProdCliente;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Precioprodcliente controller.
 *
 */
class PrecioProdClienteController extends Controller
{
    /**
     * Lists all precioProdCliente entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $precioProdClientes = $em->getRepository('ReconverpackBundle:PrecioProdCliente')->findAll();

        return $this->render('precioprodcliente/index.html.twig', array(
            'precioProdClientes' => $precioProdClientes,
        ));
    }

    /**
     * Creates a new precioProdCliente entity.
     *
     */
    public function newAction(Request $request)
    {
        $precioProdCliente = new Precioprodcliente();
        $form = $this->createForm('ReconverpackBundle\Form\PrecioProdClienteType', $precioProdCliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($precioProdCliente);
            $em->flush();

            return $this->redirectToRoute('precioprodcliente_show', array('id' => $precioProdCliente->getId()));
        }

        return $this->render('precioprodcliente/new.html.twig', array(
            'precioProdCliente' => $precioProdCliente,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a precioProdCliente entity.
     *
     */
    public function showAction(PrecioProdCliente $precioProdCliente)
    {
        $deleteForm = $this->createDeleteForm($precioProdCliente);

        return $this->render('precioprodcliente/show.html.twig', array(
            'precioProdCliente' => $precioProdCliente,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing precioProdCliente entity.
     *
     */
    public function editAction(Request $request, PrecioProdCliente $precioProdCliente)
    {
        $deleteForm = $this->createDeleteForm($precioProdCliente);
        $editForm = $this->createForm('ReconverpackBundle\Form\PrecioProdClienteType', $precioProdCliente);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('precioprodcliente_edit', array('id' => $precioProdCliente->getId()));
        }

        return $this->render('precioprodcliente/edit.html.twig', array(
            'precioProdCliente' => $precioProdCliente,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a precioProdCliente entity.
     *
     */
    public function deleteAction(Request $request, PrecioProdCliente $precioProdCliente)
    {
        $form = $this->createDeleteForm($precioProdCliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($precioProdCliente);
            $em->flush();
        }

        return $this->redirectToRoute('precioprodcliente_index');
    }

    /**
     * Creates a form to delete a precioProdCliente entity.
     *
     * @param PrecioProdCliente $precioProdCliente The precioProdCliente entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PrecioProdCliente $precioProdCliente)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('precioprodcliente_delete', array('id' => $precioProdCliente->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
