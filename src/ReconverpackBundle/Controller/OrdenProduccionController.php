<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\OrdenProduccion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Ordenproduccion controller.
 *
 */
class OrdenProduccionController extends Controller {

    /**
     * Lists all ordenProduccion entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ordenProduccions = $em->getRepository('ReconverpackBundle:OrdenProduccion')->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $ordenProduccions, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 15/* limit per page */
        );
        return $this->render('ordenproduccion/index.html.twig', array(
                    'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new ordenProduccion entity.
     *
     */
    public function newAction(Request $request, $id_pedido) {

        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('ReconverpackBundle:Pedido')->find($id_pedido);

        $orden = $em->getRepository('ReconverpackBundle:OrdenProduccion')->findOneBy(array('pedido' => $pedido));

        if ($orden == null) {
            foreach ($pedido->getProductoCliente() as $producto) {
                $cantidad = $producto->getCantidad() - $producto->getProducto()->getCantidadEnInventario();
                if($cantidad > 0){
                $ordenProduccion = new Ordenproduccion();
                $ordenProduccion->setCantidadProducir($cantidad);
                $ordenProduccion->setCantidadProdTerminado(0);
                $ordenProduccion->setPedido($pedido);
                $ordenProduccion->setProductoPedido($producto);
                $ordenProduccion->setFechaSolicitado(new \DateTime("now"));

                $em->persist($ordenProduccion);
                
                }
            }
            $pedido->setEstado('produccion');
            $em->flush();

            return $this->redirectToRoute('ordenproduccion_index');
        } else {
            return $this->redirectToRoute('ordenproduccion_show', array('id' => $orden->getId()));
        }
        /*
          $form = $this->createForm('ReconverpackBundle\Form\OrdenProduccionType', $ordenProduccion);
          $form->handleRequest($request);

          if ($form->isSubmitted() && $form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($ordenProduccion);
          $em->flush();

          return $this->redirectToRoute('ordenproduccion_show', array('id' => $ordenProduccion->getId()));
          }

          return $this->render('ordenproduccion/new.html.twig', array(
          'ordenProduccion' => $ordenProduccion,
          'form' => $form->createView(),
          ));
         * 
         */
    }

    /**
     * Finds and displays a ordenProduccion entity.
     *
     */
    public function showAction(OrdenProduccion $ordenProduccion) {
        $deleteForm = $this->createDeleteForm($ordenProduccion);

        return $this->render('ordenproduccion/show.html.twig', array(
                    'ordenProduccion' => $ordenProduccion,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ordenProduccion entity.
     *
     */
    public function editAction(Request $request, OrdenProduccion $ordenProduccion) {
        $deleteForm = $this->createDeleteForm($ordenProduccion);
        $editForm = $this->createForm('ReconverpackBundle\Form\OrdenProduccionType', $ordenProduccion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($ordenProduccion->getEstado() == 'terminado') {
                $ordenProduccion->setFechaTerminado(new \DateTime("now"));
                $ordenProduccion->getPedido()->setFechaTerminadoProduccion(new \DateTime("now"));
            }
            $this->getDoctrine()->getManager()->flush();

            //return $this->redirectToRoute('ordenproduccion_edit', array('id' => $ordenProduccion->getId()));
            return $this->redirectToRoute('ordenproduccion_index');
        }

        return $this->render('ordenproduccion/edit.html.twig', array(
                    'ordenProduccion' => $ordenProduccion,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ordenProduccion entity.
     *
     */
    public function deleteAction(Request $request, OrdenProduccion $ordenProduccion) {
        $form = $this->createDeleteForm($ordenProduccion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ordenProduccion);
            $em->flush();
        }

        return $this->redirectToRoute('ordenproduccion_index');
    }

    /**
     * Creates a form to delete a ordenProduccion entity.
     *
     * @param OrdenProduccion $ordenProduccion The ordenProduccion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OrdenProduccion $ordenProduccion) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('ordenproduccion_delete', array('id' => $ordenProduccion->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
