<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Cotizacion
 *
 * @ORM\Table(name="cotizacion")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\CotizacionRepository")
 */
class Cotizacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreCliente", type="string", length=255)
     */
    private $nombreCliente;
    /**
     * @var string
     *
     * @ORM\Column(name="emailCliente", type="string", length=255)
     */
    private $emailCliente;
    /**
     * @var string
     *
     * @ORM\Column(name="telefonoCliente", type="string", length=255, nullable=true)
     */
    private $telefonoCliente;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCreada", type="date")
     */
    private $fechaCreada;

    /**
     * @var int
     *
     * @ORM\Column(name="descuento", type="integer", nullable=true)
     */
    private $descuento;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="observacionGeneral", type="text")
     */
    private $observacionGeneral;

    /**
     * @ORM\ManyToMany(targetEntity="ProductoCotizacion", mappedBy="cotizacion", cascade="persist")
     */
    private $producto;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaCreada
     *
     * @param \DateTime $fechaCreada
     *
     * @return Cotizacion
     */
    public function setFechaCreada($fechaCreada)
    {
        $this->fechaCreada = $fechaCreada;

        return $this;
    }

    /**
     * Get fechaCreada
     *
     * @return \DateTime
     */
    public function getFechaCreada()
    {
        return $this->fechaCreada;
    }

    /**
     * Set descuento
     *
     * @param integer $descuento
     *
     * @return Cotizacion
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return int
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Cotizacion
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set observacionGeneral
     *
     * @param string $observacionGeneral
     *
     * @return Cotizacion
     */
    public function setObservacionGeneral($observacionGeneral)
    {
        $this->observacionGeneral = $observacionGeneral;

        return $this;
    }

    /**
     * Get observacionGeneral
     *
     * @return string
     */
    public function getObservacionGeneral()
    {
        return $this->observacionGeneral;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->producto = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setFechaCreada(new \DateTime('now'));
    }

    /**
     * Set nombreCliente
     *
     * @param string $nombreCliente
     *
     * @return Cotizacion
     */
    public function setNombreCliente($nombreCliente)
    {
        $this->nombreCliente = $nombreCliente;

        return $this;
    }

    /**
     * Get nombreCliente
     *
     * @return string
     */
    public function getNombreCliente()
    {
        return $this->nombreCliente;
    }

    /**
     * Set emailCliente
     *
     * @param string $emailCliente
     *
     * @return Cotizacion
     */
    public function setEmailCliente($emailCliente)
    {
        $this->emailCliente = $emailCliente;

        return $this;
    }

    /**
     * Get emailCliente
     *
     * @return string
     */
    public function getEmailCliente()
    {
        return $this->emailCliente;
    }

    /**
     * Set telefonoCliente
     *
     * @param string $telefonoCliente
     *
     * @return Cotizacion
     */
    public function setTelefonoCliente($telefonoCliente)
    {
        $this->telefonoCliente = $telefonoCliente;

        return $this;
    }

    /**
     * Get telefonoCliente
     *
     * @return string
     */
    public function getTelefonoCliente()
    {
        return $this->telefonoCliente;
    }

    /**
     * Add producto
     *
     * @param \ReconverpackBundle\Entity\ProductoCotizacion $producto
     *
     * @return Cotizacion
     */
    public function addProducto(\ReconverpackBundle\Entity\ProductoCotizacion $producto)
    {
        $this->producto[] = $producto;

        return $this;
    }

    /**
     * Remove producto
     *
     * @param \ReconverpackBundle\Entity\ProductoCotizacion $producto
     */
    public function removeProducto(\ReconverpackBundle\Entity\ProductoCotizacion $producto)
    {
        $this->producto->removeElement($producto);
    }

    /**
     * Get producto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducto()
    {
        return $this->producto;
    }
}
