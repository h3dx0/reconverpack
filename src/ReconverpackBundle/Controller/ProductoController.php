<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\Producto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Producto controller.
 *
 */
class ProductoController extends Controller
{
    /**
     * Lists all producto entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $productos = $em->getRepository('ReconverpackBundle:Producto')->findAll();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $productos, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 15/* limit per page */
        );

        return $this->render('producto/index.html.twig', array(
                    'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new producto entity.
     *
     */
    public function newAction(Request $request)
    {
        $producto = new Producto();
        $form = $this->createForm('ReconverpackBundle\Form\ProductoType', $producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $clave = $this->generarClave($producto);
            $producto->setClave($clave);
            $em = $this->getDoctrine()->getManager();
            $em->persist($producto);
            $em->flush();

            return $this->redirectToRoute('producto_index');
        }

        return $this->render('producto/new.html.twig', array(
            'producto' => $producto,
            'form' => $form->createView(),
        ));
    }
    
    private function generarClave(Producto $producto){
        $clave = '';
        $arr = explode(' ', $producto->getNombre());
        foreach ($arr as $pos){
            $clave .= substr($pos, 0, 1);
        }
        $clave .= substr($producto->getColor(), 0, 1);
        $clave .= $producto->getMedida();
        $clave .= $producto->getCantidadCapas().'C';
        
        return strtoupper($clave);
    }

    /**
     * Finds and displays a producto entity.
     *
     */
    public function showAction(Producto $producto)
    {
        $deleteForm = $this->createDeleteForm($producto);

        return $this->render('producto/show.html.twig', array(
            'producto' => $producto,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Finds and displays a producto entity.
     *
     */
    public function obtenerPrecioAction(Producto $producto)
    {
        $precio = $producto->getPrecio();

        $response = json_encode(array('rc' => 0,'precio'=>$precio));
        return new \Symfony\Component\HttpFoundation\Response($response, 200, array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * Displays a form to edit an existing producto entity.
     *
     */
    public function editAction(Request $request, Producto $producto)
    {
        $deleteForm = $this->createDeleteForm($producto);
        $editForm = $this->createForm('ReconverpackBundle\Form\ProductoEditType', $producto);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('producto_index');
        }

        return $this->render('producto/edit.html.twig', array(
            'producto' => $producto,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a producto entity.
     *
     */
    public function deleteAction(Request $request, Producto $producto)
    {
        $form = $this->createDeleteForm($producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($producto);
            $em->flush();
        }

        return $this->redirectToRoute('producto_index');
    }

    /**
     * Creates a form to delete a producto entity.
     *
     * @param Producto $producto The producto entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Producto $producto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('producto_delete', array('id' => $producto->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
