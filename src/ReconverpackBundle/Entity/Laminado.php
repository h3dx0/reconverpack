<?php

namespace ReconverpackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Laminado
 *
 * @ORM\Table(name="laminado")
 * @ORM\Entity(repositoryClass="ReconverpackBundle\Repository\LaminadoRepository")
 */
class Laminado
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidadHojas", type="integer", nullable=true)
     */
    private $cantidadHojas;

    /**
     * @var int
     *
     * @ORM\Column(name="puntosHoja", type="integer")
     */
    private $puntosHoja;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="text", nullable=true)
     */
    private $observacion;

    /**
     * @ORM\ManyToOne(targetEntity="Bobina", inversedBy="laminas")
     */
    private $bobina;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set color
     *
     * @param string $color
     *
     * @return Laminado
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set cantidadHojas
     *
     * @param integer $cantidadHojas
     *
     * @return Laminado
     */
    public function setCantidadHojas($cantidadHojas)
    {
        $this->cantidadHojas = $cantidadHojas;

        return $this;
    }

    /**
     * Get cantidadHojas
     *
     * @return int
     */
    public function getCantidadHojas()
    {
        return $this->cantidadHojas;
    }

    /**
     * Set puntosHoja
     *
     * @param integer $puntosHoja
     *
     * @return Laminado
     */
    public function setPuntosHoja($puntosHoja)
    {
        $this->puntosHoja = $puntosHoja;

        return $this;
    }

    /**
     * Get puntosHoja
     *
     * @return int
     */
    public function getPuntosHoja()
    {
        return $this->puntosHoja;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return Laminado
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set bobina
     *
     * @param \ReconverpackBundle\Entity\Bobina $bobina
     *
     * @return Laminado
     */
    public function setBobina(\ReconverpackBundle\Entity\Bobina $bobina = null)
    {
        $this->bobina = $bobina;

        return $this;
    }

    /**
     * Get bobina
     *
     * @return \ReconverpackBundle\Entity\Bobina
     */
    public function getBobina()
    {
        return $this->bobina;
    }
}
