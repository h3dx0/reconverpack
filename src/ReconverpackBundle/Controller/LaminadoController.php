<?php

namespace ReconverpackBundle\Controller;

use ReconverpackBundle\Entity\Laminado;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Laminado controller.
 *
 */
class LaminadoController extends Controller {

    /**
     * Lists all laminado entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $laminados = $em->getRepository('ReconverpackBundle:Laminado')->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $laminados, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 15/* limit per page */
        );

        return $this->render('laminado/index.html.twig', array(
                    'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new laminado entity.
     *
     */
    public function newAction(Request $request) {
        $laminado = new Laminado();
        $form = $this->createForm('ReconverpackBundle\Form\LaminadoType', $laminado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($laminado);
            $em->flush();

            return $this->redirectToRoute('laminado_index');
        }

        return $this->render('laminado/new.html.twig', array(
                    'laminado' => $laminado,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a laminado entity.
     *
     */
    public function showAction(Laminado $laminado) {
        $deleteForm = $this->createDeleteForm($laminado);

        return $this->render('laminado/show.html.twig', array(
                    'laminado' => $laminado,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing laminado entity.
     *
     */
    public function editAction(Request $request, Laminado $laminado) {
        $deleteForm = $this->createDeleteForm($laminado);
        $editForm = $this->createForm('ReconverpackBundle\Form\LaminadoType', $laminado);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('laminado_index');
        }

        return $this->render('laminado/edit.html.twig', array(
                    'laminado' => $laminado,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a laminado entity.
     *
     */
    public function deleteAction(Request $request, Laminado $laminado) {
        $form = $this->createDeleteForm($laminado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($laminado);
            $em->flush();
        }

        return $this->redirectToRoute('laminado_index');
    }

    /**
     * Creates a form to delete a laminado entity.
     *
     * @param Laminado $laminado The laminado entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Laminado $laminado) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('laminado_delete', array('id' => $laminado->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
